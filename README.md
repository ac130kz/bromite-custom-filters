# bromite-custom-filters

Automatically updated filters for Bromite browser

## How to install?
Put the following as the "Filters URL" in the settings
```
https://gitlab.com/ac130kz/bromite-custom-filters/-/jobs/artifacts/master/raw/filters.dat?job=build
```

## References
```
https://github.com/bromite/bromite
https://github.com/uBlockOrigin/uAssets
https://www.bromite.org/custom-filters
```